require('dotenv').config()

const https = require('https')
const http = require('http')
const fs = require('fs')
const express = require('express')
const proxy = require('http-proxy-middleware')

const app = express()
const insecureApp = express()

const options = {
  key: fs.readFileSync(process.env.PRIVATEKEY_FILE || 'key.pem'),
  cert: fs.readFileSync(process.env.CERTIFICATE_FILE || 'cert.pem'),
  passphrase: process.env.PASSPHRASE || ''
}

insecureApp.use((req, res) => {
  res.redirect(`https://${req.headers.host}${req.url}`)
})

const pathRewrite = {}
pathRewrite['^' + (process.env.API_PATH || '/api/web')] = ''

app.use(process.env.API_PATH || '/api/web/', proxy({
  target: process.env.API_HOST || 'http://localhost:3000',
  changeOrigin: true,
  ws: true,
  pathRewrite
}))

app.use(proxy({
  target: process.env.CLIENT_HOST || 'http://localhost:8080',
  changeOrigin: true,
  ws: true
}))

http
  .createServer(insecureApp)
  .listen(80, process.env.HOST || '127.0.0.1')

https
  .createServer(options, app)
  .listen(443, process.env.HOST || '127.0.0.1')
