require('dotenv').config()

const http = require('http')
const fs = require('fs')
const express = require('express')
const proxy = require('http-proxy-middleware')

const app = express()

const pathRewrite = {}
pathRewrite['^' + (process.env.API_PATH || '/api/web')] = ''

app.use(process.env.API_PATH || '/api/web/', proxy({
  target: process.env.API_HOST || 'http://localhost:3000',
  changeOrigin: true,
  ws: true,
  pathRewrite
}))

app.use(proxy({
  target: process.env.CLIENT_HOST || 'http://localhost:8080',
  changeOrigin: true,
  ws: true
}))

http
  .createServer(app)
  .listen(process.env.PORT || 1337, process.env.HOST || '127.0.0.1')
